<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>

    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        @csrf
        <h3>Sign Up Form</h3>
        <label>Firts name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="fname2"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" value="1" name="skill">Male <br>
        <input type="radio" value="2" name="skill">Female <br>
        <input type="radio" value="3" name="skill">Other <br><br>
        <label>Nationality</label><br><br>
        <select name="negara" id="negara">
            <option value="1">Indonesian</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="Language">Bahasa Indonesia <br>
        <input type="checkbox" name="Language">English <br>
        <input type="checkbox" name="Language">Other <br>
        <br>
        <label>Bio :</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="kirim">
    </form>
</body>

</html>