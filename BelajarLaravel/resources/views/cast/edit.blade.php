@extends('layouts.master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf 
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" value="{{$cast->nama}}" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Silahkan Masukan Nama Anda">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" value="{{$cast->umur}}" class="@error('umur') is-invalid @enderror form-control" placeholder="Silahkan Masukan Umur Anda">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Biodata</label>
    <textarea class="@error('bio') is-invalid @enderror form-control" value="{{$cast->bio }}" name="bio" cols="30" rows="10" placeholder="Silahkan Masukan Biodata Anda">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection