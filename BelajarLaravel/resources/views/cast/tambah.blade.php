@extends('layouts.master')
@section('title')
Halaman Tambah Cast
@endsection
@section('content')

<form action="/cast" method="POST">
    @csrf 
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Silahkan Masukan Nama Anda">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" class="@error('umur') is-invalid @enderror form-control" placeholder="Silahkan Masukan Umur Anda">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Biodata</label>
    <textarea class="@error('bio') is-invalid @enderror form-control" name="bio" cols="30" rows="10" placeholder="Silahkan Masukan Biodata Anda"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection