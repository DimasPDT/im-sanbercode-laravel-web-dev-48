<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'form']);
Route::post('/welcome', [AuthController::class, 'kirim']);
Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.datatable');
});

// Route::get('/master', function(){
//     return view('layouts.master');
// });


// CRUD

// CREATE DATA
// ROUTE YANG MENGARAH KE FORM TAMBAH DATA 
Route::get('/cast/create', [CastController::class, 'create']);

// ROUTE UNTUK TAMBAH DATA KE DATABASE
Route::post('/cast', [CastController::class, 'store']);

// ROUTE UNTUK READ DATA KE DATABASE
// ROUTE UNTUK MENAMPIL SEMUA KATEGORI DATABASE
Route::get('/cast', [CastController::class, 'index']);

//ROUTE UNTUK MENAMPILKAN DETAIL CAST BERDASARKAN ID
Route::get('/cast/{id}', [CastController::class, 'show']);


// ROUTE UNTUK MENGARAH KE FORM EDIT DATA DENGAN PARAMETER ID
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// ROUTE UPDATE DATA BERDASARKAN GUBERNU
Route::put('/cast/{id}', [CastController::class, 'update']);

//DELET DATA 
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
