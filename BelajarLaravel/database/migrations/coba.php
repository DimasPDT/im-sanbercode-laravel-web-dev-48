user
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->timestamps();


            profile
            $table->id();
            $table->string('umur');
            $table->text('bio');
            $table->text('alamat');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('user');
            $table->timestamps();

            kritik
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('user');
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            $table->text('content');
            $table->integer('point');
            $table->timestamps();

            cast
            $table->id();
            $table->string('nama');
            $table->integer('umur');
            $table->text('bio');
            $table->timestamps();

            peran
            $table->id();
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->string('nama');
            $table->timestamps();

            film
            $table->id();
            $table->string('judul');
            $table->text('ringkasan');
            $table->integer('tahun');
            $table->string('poster');
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre');
            $table->timestamps();

            genre
            $table->id();
            $table->string('nama');
            $table->timestamps();